var listElement = document.querySelector("#app ul");
var inputElemnt = document.querySelector("#app input");
var buttonElement = document.querySelector("#app button");

// SE A LISTA JSON ESTIVER VAZIA E FOR A PRIMEIRA VEZ, CRIA UM ARRAY PARA PODER ADCIONAR OS TODOS
var todos = JSON.parse(localStorage.getItem('list_todos')) || [];

function renderTodos(){
    listElement.innerHTML = '';

    for(todo of todos){
        //cria a lista
        var todoElement = document.createElement('li');
        //Adciona o nome à lista atual
        var todoText = document.createTextNode(todo);

        //cria o elemento de excluir
        var linkElement = document.createElement('a');
        //adciona o atributo href em "excluir"
        linkElement.setAttribute('href', '#');
        //pega a index atual do for
        var pos = todos.indexOf(todo);        
        //adciona o onclick no "excluir"
        linkElement.setAttribute('onclick','deleteTodo('+pos+')');
        //Adciona o campo escrito Excluir no front
        var linkText = document.createTextNode(' Excluir');
        
        //Amarra o "Excluir no elemento <a> de ecluir"
        linkElement.appendChild(linkText)

        //Passa o valor do input para uma nova posição na lista
        todoElement.appendChild(todoText);
        //Amarra o botão de excluir ao valor atual da lista
        todoElement.appendChild(linkElement);
        //Adciona de fato a valor na lista
        listElement.appendChild(todoElement);
    }
}

function addTodo(){

    var todoText = inputElemnt.value;

    todos.push(todoText);
    inputElemnt.value = '';
    renderTodos();
    saveToStorage()

}

function deleteTodo(pos){
    todos.splice(pos,1);
    renderTodos();
    saveToStorage()
}

function saveToStorage(){
    localStorage.setItem('list_todos',JSON.stringify(todos));
}

renderTodos();

buttonElement.onclick = addTodo;